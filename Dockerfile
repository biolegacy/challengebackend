FROM node:12.9.1

RUN mkdir -p /usr/src/app

WORKDIR /usr/src/app

COPY package*.json ./
COPY tsconfig.json ./

COPY empty.ts ./

RUN npm install --no-cache
RUN npm run build

COPY /dist .

EXPOSE 8000

CMD ["npm", "run", "start:in:docker"]