import mongoose from "mongoose";

export const Init = () => {
        mongoose.connect(`mongodb://mongo:27017/challenge`, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: true, authSource:"admin"  })
        // In case of testing it in local docker environment and want to connect to the locally running mongoDB
        // mongoose.connect(`mongodb://host.docker.internal:27017/docker-travel-hairdresser`, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: true, authSource:"admin"  })
    
    .then(() => {
        console.log("Connected to MongoDB!");
    })
    .catch((e) => {
        console.log("Error: ", e)
        // Try to reconnect to the DB every 15 seconds
        setTimeout(()=>{
            Init();
        }, 15000)
    })
}
