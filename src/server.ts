// Importing components
import express from 'express';
import bodyParser from 'body-parser';
import Routes from "./routes";
import { createServer } from 'http';
import * as DB from "./db";

const Port = 8000;
// Set the server basic configurations here
const app = express();

// Initialize the DB connection
DB.Init();

app.use(bodyParser.urlencoded({
    extended:true
}));
  
app.use(bodyParser.json());

// Set the routes
app.use(`/api`, Routes);

app.listen(
    { port: Port },
    () => console.log(`\n🚀      Server is now running on http://localhost:8000/`)
);